import React, { Component} from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
import ContextData from './components/ContextData';







class App extends Component {
  
  render() {

    return (
      <div className="App">
        <BrowserRouter>
          <ContextData />
        </BrowserRouter>
      </div>
    );
  }
}
export default App;

