import React,{Component} from "react";

class DisplayCartItems extends Component {

    handlepostData = (data) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        };
        fetch('http://localhost:8000/order', requestOptions)
            .then(res => res.json())
            .then(data => console.log(data));
        this.props.value.resetTheData();
    }
   render() {
       const { value } = this.props;
       const total = value.data.reduce((acc, curr) => acc += curr.quantity, 0);
       return (
        <div>
        { total > 0 ? <div>
            {value.data?.map((item, index) =>

                <div key={index}>

                    {item.quantity > 0 ?
                        <div className='items'>
                            <div>
                                <div className='item'>
                                    <img src={item.image} alt='Ice'></img>
                                    <p className='Para'>{item.name}</p>
                                    <p className='Para'>Rs.{item.price}</p>
                                </div>
                                <div className='btn'>
                                    <div>
                                        <button className='btn-buy' onClick={() => { value.handleDecrement(index) }}>-</button>
                                        <button className='btn-buy'>{item.quantity}</button>
                                        <button className='btn-buy' onClick={() => { value.handleIncrement(index) }}>+</button>
                                    </div>
                                </div>
                            </div>
                        </div> : ""}

                </div>)}
            <button className='btn-buy' onClick={() => (this.handlepostData(value.data))}>Place Order</button>
        </div> : <img className='emptycart'src='../images/cartEmpty.png' alt='cartEmptyImage'></img>}
    </div>
       )
   }
}

export default DisplayCartItems