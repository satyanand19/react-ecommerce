import React from 'react';
import { useParams } from 'react-router-dom';

function withParams(Component) {
    return (props) => {
        const params = useParams();

        return <Component {...props} params={params}      />
    }
}



// function withHoc (Component) {
//     return () => {
//         const [name, updateName] u= us
//        return <Component />
//     }
// }


export default withParams;