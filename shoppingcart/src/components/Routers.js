import React, { Component } from "react";
import { Routes, Route } from 'react-router-dom'
import Home from './Home';
import Carts from './Cart';
import Order from "./order";
import Details from "./Details";
// import GetParams from "./GetParams";
// import Order from "./order";

class RoutesOfpage extends Component {
    render() {
        return (
            
            <Routes>
            <Route path='/' element={<Home  />} ></Route>
            <Route path='/carts' element={<Carts  />}></Route>
            <Route path='/order' element={<Order></Order>}></Route>
            <Route path='/products/:id' element={<Details />}></Route>
          </Routes>
        )
    }

}

export default RoutesOfpage