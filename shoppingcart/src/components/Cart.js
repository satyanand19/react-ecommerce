import React, { Component } from "react";
// import { Link } from 'react-router-dom';
import { DataContext } from './ContextData.js';
import DisplayCartItems from "./displayCartItems.js";

class Carts extends Component {

    

    render() {
        //    const {data} = this.props.location;
        return (
            <div>
                <DataContext.Consumer>
                    {(value) => (
                        <DisplayCartItems value={value}></DisplayCartItems>
                    )
                    }
                </DataContext.Consumer>
            </div>
        )
    }
}

export default Carts