import React,{Component, createContext } from 'react'
import Navigation from './Navigation';
import RoutesOfpage from './Routers';
const DataContext = createContext();

class ContextData extends Component {
    constructor() {
        super()
        this.state = {
          data: []
        }
      }
      componentDidMount() {
        fetch('http://localhost:8000/')
          .then(response => response.json())
          .then(data => {
            this.setState({ data: data["response"]['data'] })
          });
      }
      handleIncrement = (index) => {
        console.log(this.state.data);
    
        // let array = (this.state.data);
        // array[index]['quantity'] += 1;
        this.setState(preState => ({
          data: [...preState.data.slice(0,index),{...preState.data[index],quantity:preState.data[index].quantity+1},...preState.data.slice(index+1)]
        }))
      }
      handleDecrement = (index) => {
        this.setState(preState => ({
          data: [...preState.data.slice(0,index),{...preState.data[index],quantity:preState.data[index].quantity-1},...preState.data.slice(index+1)]
        }))
      }
      resetTheData =() => {
        let array = [...this.state.data];
        let newValue = array.map(item => {
          return {...item,quantity:0};
        });
        this.setState({data : newValue})
    }
   render() {
       return (
           <>
        <Navigation />
        <DataContext.Provider value={{
          data: this.state.data,
          handleDecrement: this.handleDecrement,
          handleIncrement: this.handleIncrement,
          resetTheData : this.resetTheData
        }}>
          <RoutesOfpage />
        </DataContext.Provider>
        </>
       )
   }
}

export default ContextData
export { DataContext };