import React, { Component } from "react";
import WithParams from "./GetParams";
// import { DataContext } from '../App.js';

class Details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {}
        }
    }
    componentDidMount() {
        fetch(`http://localhost:8000/products/${this.props.params.id}`)
            .then(response => response.json())
            .then(data => {
                this.setState({ data: data })
            });
    }
    render() {
        const { image , name, price} = this.state.data;
        return (
            <div className="items">
                <div className='item'>
                    <img src={image} alt='Ice'></img>
                    <p className='Para'>{name}</p>
                    <p className='Para'>Rs.{price}</p>
                </div>
            </div>
        )
    }
}

export default WithParams(Details);
