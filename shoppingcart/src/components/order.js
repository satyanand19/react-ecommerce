import React, { Component } from "react";


class Order extends Component {
    constructor(props) {
      super(props)
      this.state = { 
          data : []
      }
    }
    componentDidMount() {
        fetch('http://localhost:8000/order')
        .then(response => response.json())
        .then(data => {
                this.setState({data : (data)})
        })
    }
    render() {
        return (
            <div> {this.state.data.map(order => <div className='items'key={Object.keys(order)[0]}>
                <h1 className="orderId">OrderId : {Object.keys(order)[0]}</h1>
                {
                order[Object.keys(order)[0]].map(item => <div key={item.name}>
                    {item.quantity > 0 ? 
                <div className="t">
                    
                    <span className="span">{item.name}</span>
                    <span className="order">
                    <span className="span">{item.price}</span>
                    <span className="span">X</span>
                    <span className="span">{item.quantity}</span>
                    <span className="span">=</span>
                    <span className="span">{item.quantity*item.price}</span> 
                    </span>
                </div> :<></>}
                </div>)}
                <h2 className="total">Grand Total : {order[Object.keys(order)[0]].reduce((acc,curr) => acc += curr.quantity*curr.price,0)}</h2>
            </div>)}</div>
        )
    }
}

export default Order;
