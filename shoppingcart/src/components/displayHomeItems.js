import React,{Component} from 'react'
import { Link } from 'react-router-dom';


class DisplayHomeItems extends Component {
   render() {
       const { value } =this.props;
       return (
        <div>
                           
        <h1>IceCream</h1>
        <div className='container'>
        {value.data?.map((item, index) =>
            <div className='items' key={index}>
                <Link to={'/Products/'+index}>
                <div className='item'>
                    <img src={item.image} alt='Ice'></img>
                    <p className='Para'>{item.name}</p>
                    <p className='Para'>Rs.{item.price}</p>
                </div></Link>
                <div className='btn'>
                    {item.quantity === 0 ?
                        <button className='btn-buy' onClick={() => { value.handleIncrement(index) }}>Add Items to Cart</button>
                        : <div>
                            <button className='btn-buy' onClick={() => { value.handleDecrement(index) }}>-</button>
                            <button className='btn-buy' id='counter'>{item.quantity}</button>
                            <button className='btn-buy' onClick={() => { value.handleIncrement(index) }}>+</button>
                        </div>}
                </div>
                
            </div>)}
            </div>
    </div>
       )
   }
}

export default DisplayHomeItems