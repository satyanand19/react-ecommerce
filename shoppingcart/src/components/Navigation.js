import React, { Component } from "react";
import { Link } from 'react-router-dom'

class Navigation extends Component {
    render() {
        return (
            <nav className="nav">
                <Link to='/' className="nav-link">Home</Link>
                <Link to='/carts' className="nav-link">Cart Items</Link>
                <Link to='/order' className="nav-link">Order</Link>
            </nav>
        )
    }

}

export default Navigation